from config.settings import MEDIA_ROOT


def format_records(records: list) -> str:
    return "<br>".join(str(record) for record in records)


def query_dict_formater(query_dic):  # Convert QueryDict to regular dict
    return {key: value[0] for key, value in query_dic.lists()}


def directory_path(instance, filename):
    return f"{MEDIA_ROOT}/user_{instance.pk}/{filename}"
