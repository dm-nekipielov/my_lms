from django.contrib.auth import login, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import TemplateView, CreateView, RedirectView, DetailView, UpdateView

from core.forms import RegistrationForm, ProfileForm
from core.models import Profile
from core.services.emails import send_registration_email
from core.utility.token_generator import TokenGenerator
from students.models import Student


class IndexView(TemplateView):
    template_name = "index.html"


class Registration(CreateView):
    template_name = "registration/create_user.html"
    form_class = RegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()

        send_registration_email(
            self.request, user_instance=self.object
        )

        return super().form_valid(form)


class ActivateUser(RedirectView):
    url = reverse_lazy("index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return HttpResponse("Wrong data!")

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()
            login(request, current_user)

            return super().get(request, *args, **kwargs)

        return HttpResponse("Wrong data!")


class Login(LoginView):
    next_page = reverse_lazy("index")


class Logout(LogoutView):
    next_page = reverse_lazy("login")


def search(request):
    if request.method == "POST":
        searched = request.POST["searched"]
        students = Student.objects.filter(Q(first_name__contains=searched) | Q(last_name__contains=searched))
        print(students)
        return render(request, template_name="search.html", context={"searched": searched, "students": students})

    return render(request, template_name="search.html", context={})


class ProfileView(LoginRequiredMixin, DetailView):
    model = Profile
    template_name = "profile.html"


class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    model = Profile
    form_class = ProfileForm
    template_name = "profile-update.html"
    login_url = "login"

    def get_success_url(self):
        return reverse_lazy('profile', kwargs={'pk': self.kwargs['pk']})
