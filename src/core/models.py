from datetime import datetime
from uuid import uuid4

from django.contrib.auth import get_user_model
from django.core.validators import FileExtensionValidator, MinLengthValidator
from django.db import models
from location_field.models.plain import PlainLocationField
from phonenumber_field.modelfields import PhoneNumberField

from utils import directory_path


class Person(models.Model):
    class Meta:
        abstract = True

    uuid = models.UUIDField(
        primary_key=True,
        editable=False,
        default=uuid4,
        unique=True,
        db_index=True,
    )
    avatar = models.ImageField(
        upload_to=directory_path,
        validators=[FileExtensionValidator(allowed_extensions=['jpg', 'jpeg', 'png'])],
        null=True,
        blank=True,
        default='')
    first_name = models.CharField(
        max_length=100,
        validators=[
            MinLengthValidator(2),
        ],
        default=''
    )
    last_name = models.CharField(
        max_length=100,
        validators=[
            MinLengthValidator(2),
        ],
        default=''
    )
    email = models.EmailField(max_length=100, default='')
    birth_date = models.DateField(null=True, default='')

    @property
    def get_photo_url(self):
        if self.avatar and hasattr(self.avatar, 'url'):
            return self.avatar.url
        else:
            return "/media/avatar.jpeg"

    def age(self):
        return datetime.now().year - self.birth_date.year


class Profile(models.Model):
    STUDENT = "Student"
    TEACHER = "Teacher"
    MENTOR = "Mentor"
    USER_TYPE_CHOICES = (
        (STUDENT, "Student"),
        (TEACHER, "Teacher"),
        (MENTOR, "Mentor"),
    )

    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    avatar = models.ImageField(
        upload_to=directory_path,
        validators=[FileExtensionValidator(allowed_extensions=['jpg', 'jpeg', 'png'])],
        null=True,
        blank=True,
        default="/media/avatar.jpeg"
    )
    birth_date = models.DateField(null=True, default='')
    phone_number = PhoneNumberField()
    facebook = models.URLField(max_length=255, null=True, blank=True, default='')
    github = models.URLField(max_length=255, null=True, blank=True, default='')
    city = models.CharField(max_length=255, null=True, blank=True, default='')
    location = PlainLocationField(based_fields=['city'], zoom=7, null=True, blank=True, default='')
    user_type = models.CharField(max_length=50, choices=USER_TYPE_CHOICES)

    @property
    def get_photo_url(self):
        if self.avatar and hasattr(self.avatar, 'url'):
            return self.avatar.url
        else:
            return "/media/avatar.jpeg"

    def __str__(self):
        return f"{self.user.username} {self.user.first_name} {self.user.last_name} {self.user.email}"
