from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from django.forms import ModelForm, DateField, DateInput

from core.models import Profile


class PersonForm(ModelForm):
    birth_date = DateField(
        input_formats=["%d.%m.%Y", "%Y-%m-%d"],
        widget=DateInput(attrs={"placeholder": "dd.mm.yyyy", "type": "date"}),
    )

    @staticmethod
    def normalize_text(text: str) -> str:
        return text.strip().capitalize()

    def clean_email(self):
        email = self.cleaned_data["email"]

        if "@yandex" in email.lower():
            raise ValidationError("Yandex domain is forbidden in our country")

        return email

    def clean_first_name(self):
        return self.normalize_text(self.cleaned_data["first_name"])

    def clean_last_name(self):
        return self.normalize_text(self.cleaned_data["last_name"])

    def clean(self):
        cleaned_data = super().clean()

        first_name = cleaned_data["first_name"]
        last_name = cleaned_data["last_name"]

        if first_name == last_name:
            raise ValidationError("First name and last name can`t be equal")

        return cleaned_data


class RegistrationForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ["first_name", "last_name", "username", "email", "password1", "password2"]


class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ["avatar", "birth_date", "phone_number", "city", "facebook", "github", "user_type"]
