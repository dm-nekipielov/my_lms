import random

from django.db import models
from faker import Faker


class Group(models.Model):
    course_name = models.CharField(max_length=100, default='')
    start_date = models.DateField(null=True)
    group_name = models.CharField(max_length=100, default='')
    lessons = models.IntegerField(null=True, default=0)
    students_qty = models.IntegerField(null=True, default=0)

    @classmethod
    def generate_groups(cls, count):
        courses = [
            "Python Basic",
            "Python Pro",
            "Machine Learning",
            "Java Enterprise",
            "Java Pro",
            "Java Basic",
            "PHP",
            "PHP Basic",
            "QA Manual",
            "QA Automation",
        ]
        fake = Faker("uk_UA")

        for _ in range(count):
            cls.objects.create(
                course_name=random.choice(courses),
                start_date=fake.date_time_between(start_date="+1d", end_date="+2m"),
                group_name=f"hillel_({fake.ean(length=8)})",
                lessons=random.randint(16, 32),
                students_qty=random.randint(12, 16),
            )

    def __str__(self):
        return f"{self.course_name} {self.start_date} {self.group_name}"
