from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from groups.forms import GroupForm
from groups.models import Group


class GroupListView(LoginRequiredMixin, ListView):
    model = Group
    template_name = "groups_list.html"
    login_url = "login"


class CreateGroupView(LoginRequiredMixin, CreateView):
    model = Group
    form_class = GroupForm
    template_name = "create.html"
    success_url = reverse_lazy("groups:get_groups")
    login_url = "login"


class UpdateGroupView(LoginRequiredMixin, UpdateView):
    model = Group
    form_class = GroupForm
    template_name = "update.html"
    success_url = reverse_lazy("groups:get_groups")
    login_url = "login"


class DeleteGroupView(LoginRequiredMixin, DeleteView):
    model = Group
    template_name = "delete.html"
    success_url = reverse_lazy("groups:get_groups")
    login_url = "login"
