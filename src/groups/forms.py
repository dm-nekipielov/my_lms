from django.forms import ModelForm, DateField, DateInput

from groups.models import Group


class GroupForm(ModelForm):
    start_date = DateField(
        input_formats=["%d.%m.%Y", "%Y-%m-%d"],
        widget=DateInput(attrs={"placeholder": "dd.mm.yyyy", "type": "date"}),
    )

    class Meta:
        model = Group
        fields = "__all__"

    @staticmethod
    def normalize_text(text: str) -> str:
        return text.strip().capitalize()

    def clean_course_name(self):
        return self.normalize_text(self.cleaned_data["course_name"])

    def clean_group_name(self):
        return self.normalize_text(self.cleaned_data["group_name"])
