# Generated by Django 4.1.1 on 2022-09-06 11:15

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Group",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("course_name", models.CharField(default="", max_length=100)),
                ("start_date", models.DateField(null=True)),
                ("group_name", models.CharField(default="", max_length=100)),
                ("lessons", models.IntegerField(default=0, null=True)),
                ("students_qty", models.IntegerField(default=0, null=True)),
            ],
        ),
    ]
