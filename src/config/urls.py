"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from core.views import IndexView, search, Login, Logout, Registration, ActivateUser, ProfileView, ProfileUpdateView

urlpatterns = [
                  path("", IndexView.as_view(), name="index"),
                  path("admin/", admin.site.urls),
                  path("search/", search, name="search"),
                  path("students/", include("students.urls")),
                  path("teachers/", include("teachers.urls")),
                  path("groups/", include("groups.urls")),
                  path("login/", Login.as_view(), name="login"),
                  path("logout/", Logout.as_view(), name="logout"),
                  path("registration/", Registration.as_view(), name="registration"),
                  path("activate/<str:uuid64>/<str:token>", ActivateUser.as_view(), name="activate_user"),
                  path("profile/<int:pk>", ProfileView.as_view(), name="profile"),
                  path("profile/<int:pk>/update", ProfileUpdateView.as_view(), name="profile-update"),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
