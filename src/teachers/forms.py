from django.forms import ModelForm, DateField, DateInput, RegexField

from teachers.models import Teacher


class TeacherForm(ModelForm):
    phone = RegexField(
        regex=r"^\+?\d{12}$",
        error_messages={"invalid": "Phone number must be in format: +999999999999"},
    )
    birth_date = DateField(
        input_formats=["%d.%m.%Y", "%Y-%m-%d"],
        widget=DateInput(attrs={"placeholder": "dd.mm.yyyy", "type": "date"})
    )

    class Meta:
        model = Teacher
        fields = "__all__"
