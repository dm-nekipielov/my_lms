from django.urls import path

from teachers.views import CreateTeacherView, TeacherListView, UpdateTeacherView, DeleteTeacherView

app_name = "teachers"

urlpatterns = [
    path("", TeacherListView.as_view(), name="get_teachers"),
    path("create/", CreateTeacherView.as_view(), name="create_teacher"),
    path("update/<uuid:pk>/", UpdateTeacherView.as_view(), name="update_teacher"),
    path("delete/<uuid:pk>/", DeleteTeacherView.as_view(), name="delete_teacher"),
]
