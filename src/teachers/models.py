import random

from django.db import models
from faker import Faker

from core.models import Person
from groups.models import Group


class Teacher(Person):
    phone = models.CharField(max_length=13, null=True, default='')
    course = models.CharField(max_length=100, null=True, default='')
    group = models.ManyToManyField(to=Group)

    @classmethod
    def generate_teachers(cls, count):
        courses = [
            "Python Basic",
            "Python Pro",
            "Machine Learning",
            "Java Enterprise",
            "Java Pro",
            "Java Basic",
            "PHP",
            "PHP Basic",
            "QA Manual",
            "QA Automation",
        ]
        fake = Faker("uk_UA")

        for _ in range(count):
            cls.objects.create(
                first_name=fake.first_name(),
                last_name=fake.last_name(),
                birth_date=fake.date_time_between(start_date="-50y", end_date="-25y"),
                email=fake.email(),
                phone=fake.phone_number(),
                course=random.choice(courses),
            )

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} {self.course}"
