from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import DeleteView, UpdateView, ListView, CreateView

from teachers.forms import TeacherForm
from teachers.models import Teacher


class TeacherListView(LoginRequiredMixin, ListView):
    model = Teacher
    template_name = "teachers-list.html"
    login_url = "login"


class CreateTeacherView(LoginRequiredMixin, CreateView):
    model = Teacher
    form_class = TeacherForm
    template_name = "create.html"
    success_url = reverse_lazy("teachers:get_teachers")
    login_url = "login"


class UpdateTeacherView(LoginRequiredMixin, UpdateView):
    model = Teacher
    form_class = TeacherForm
    template_name = "update.html"
    success_url = reverse_lazy("teachers:get_teachers")
    login_url = "login"


class DeleteTeacherView(LoginRequiredMixin, DeleteView):
    model = Teacher
    template_name = "delete.html"
    success_url = reverse_lazy("teachers:get_teachers")
    login_url = "login"
