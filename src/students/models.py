from django.core.validators import FileExtensionValidator
from django.db import models
from faker import Faker

from core.models import Person
from groups.models import Group
from utils import directory_path


class Student(Person):
    grade = models.PositiveSmallIntegerField(null=True, default=0)
    group = models.ForeignKey(
        to=Group,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    cv = models.FileField(
        upload_to=directory_path,
        validators=[FileExtensionValidator(allowed_extensions=['pdf'])],
        null=True,
        blank=True,
        default='')

    @classmethod
    def generate_students(cls, count):
        faker = Faker("uk_UA")

        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_time_between(start_date="-30y", end_date="-18y"),
            )

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email}"
