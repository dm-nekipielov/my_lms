from django.urls import path

from students.views import StudentListView, CreateStudentView, UpdateStudentView, DeleteStudentView

app_name = "students"

urlpatterns = [
    path("", StudentListView.as_view(), name="all_students"),
    path("create/", CreateStudentView.as_view(), name="create_student"),
    path("update/<uuid:pk>/", UpdateStudentView.as_view(), name="update_student"),
    path("delete/<uuid:pk>/", DeleteStudentView.as_view(), name="delete_student"),
]
