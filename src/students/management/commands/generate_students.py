from django.core.management.base import BaseCommand
from faker import Faker

fake = Faker()


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("quantity", type=int)

    def handle(self, *args, **kwargs):
        quantity = kwargs["quantity"]
        for _ in range(quantity):
            print(
                {
                    "First_name": fake.first_name(),
                    "Last_name": fake.last_name(),
                    "Email": fake.unique.email(),
                    "Birthday": str(fake.date_of_birth(minimum_age=18, maximum_age=45)),
                }
            )
