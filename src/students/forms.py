from core.forms import PersonForm
from students.models import Student


class StudentForm(PersonForm):
    class Meta:
        model = Student
        fields = "__all__"
