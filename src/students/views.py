from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, ListView

from .forms import StudentForm
from .models import Student


class StudentListView(LoginRequiredMixin, ListView):
    model = Student
    template_name = "students_list.html"
    login_url = "login"


class CreateStudentView(LoginRequiredMixin, CreateView):
    model = Student
    form_class = StudentForm
    template_name = "create.html"
    success_url = reverse_lazy("students:all_students")
    login_url = "login"


class UpdateStudentView(LoginRequiredMixin, UpdateView):
    model = Student
    form_class = StudentForm
    template_name = "update.html"
    success_url = reverse_lazy("students:all_students")
    login_url = "login"


class DeleteStudentView(LoginRequiredMixin, DeleteView):
    model = Student
    template_name = "delete.html"
    success_url = reverse_lazy("students:all_students")
    login_url = "login"
